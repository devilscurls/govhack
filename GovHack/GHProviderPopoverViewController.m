//
//  GHProviderPopoverViewController.m
//  GovHack
//
//  Created by Lewis Daly on 2/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHProviderPopoverViewController.h"

@interface GHProviderPopoverViewController ()

@end

@implementation GHProviderPopoverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
    //TODO:Format the view here...
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) resize
{
    //Figure out how big we need to be!
    CGSize size = [self.nameLabel.text sizeWithFont:self.nameLabel.font];
    self.view.frame = CGRectMake(0, 0, size.width + 100, 225);
    
    
    
    
    
}

- (void) loadProvider:(GHProvider *)provider
{
    self.nameLabel.text = provider.name;
    self.email.text = provider.email;
    self.site.text = provider.urlString;
    self.phone.text = provider.workPhone;
    
    self.skillsForAll.text = provider.skillsForAllApproved ? @"YES" : @"NO";
    
    
    
}

@end
