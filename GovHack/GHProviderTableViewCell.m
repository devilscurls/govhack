//
//  GHProviderTableViewCell.m
//  GovHack
//
//  Created by Lewis Daly on 2/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHProviderTableViewCell.h"

@implementation GHProviderTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    self.backgroundColor = [UIColor clearColor];
    self.cellBackgroundView.backgroundColor = [UIColor blackColor];
    self.cellBackgroundView.alpha = 0.5;
    self.cellLabel.textColor = [UIColor whiteColor];
    
//    self.backgroundView.layer.cornerRadius = 5;
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
