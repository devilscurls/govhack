//
//  GHCoursesViewController.m
//  GovHack
//
//  Created by Lewis Daly on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHCoursesViewController.h"
#import "GHProvidersViewController.h"
#import "GHProviderPopoverViewController.h"

#define HEADER_HEIGHT 64

@interface GHCoursesViewController ()

@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, strong) GHProviderPopoverViewController *popoverContent;


@end

@implementation GHCoursesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"Courses";
    
    //Delegates
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    self.courseNameLabel.minimumScaleFactor = 0.5;
    self.courseNameLabel.adjustsFontSizeToFitWidth = YES;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    //Select the 1st cell
    self.currentCourse = [_courses objectAtIndex:0];
    [self.tableView reloadData];
    
    //Set up Labels:
    self.courseNameLabel.text = [_currentCourse.name uppercaseString];
    self.courseCodeLabel.text = [_currentCourse.code uppercaseString];
    self.courseStatusCode.text = [_currentCourse.statusCode uppercaseString];
    self.courseFeeStatus.text = _currentCourse.feeFreeFlag ? @"YES" : @"NO";
    
    self.popoverContent = [self.storyboard instantiateViewControllerWithIdentifier:@"GHProviderPopoverViewController"];
    self.popover = [[UIPopoverController alloc] initWithContentViewController:_popoverContent];
}


- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions
- (IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - Prepare For Segue

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowProviders"])
    {
        GHProvidersViewController *providersVC = segue.destinationViewController;
        NSIndexPath *indexPath = [[_collectionView indexPathsForSelectedItems] lastObject];
        providersVC.currentCourse = [_courses objectAtIndex:indexPath.row];
    }
    
}

#pragma mark - UITableViewDatasource
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //TODO: Return a number, depending on collectionview selections
    return _currentCourse.providers.count;
    
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Display popover (//TODO: get frame of selected row
    GHProvider *provider = [_currentCourse.providers objectAtIndex:indexPath.row];
    [_popoverContent loadProvider: provider];
    [_popoverContent resize];

    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    float x = _tableView.frame.origin.x;
    float y = _tableView.frame.origin.y;
    CGRect frame = CGRectMake(x + cell.frame.origin.x, y + cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);

    [_popover setPopoverContentSize:_popoverContent.view.frame.size];
    [_popover presentPopoverFromRect:frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];

    
    
    
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GHProviderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GHProviderTableViewCell" forIndexPath:indexPath];
    
    //Get the provider:
    GHProvider *provider = [_currentCourse.providers objectAtIndex:indexPath.row];
    cell.cellLabel.text = [provider.name capitalizedString];
    
    return cell;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //Create the header view
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, HEADER_HEIGHT)];
    view.backgroundColor = [UIColor clearColor];
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, HEADER_HEIGHT - 4)];
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0.5f;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, tableView.frame.size.width - 20, HEADER_HEIGHT)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:20];
    label.text = @"PROVIDERS";

    [view addSubview:backgroundView];
    [view addSubview:label];
    return view;
}

- (float) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HEADER_HEIGHT;
}

#pragma mark - UICollectionView Datasource
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return _courses.count;
    
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 1;
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GHCourseCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"GHCourseCell" forIndexPath:indexPath];
    
    //Get the course
    GHCourse *course = [_courses objectAtIndex:indexPath.row];
    cell.titleLabel.text = course.name;
    
    return cell;
}

// 4
/*- (UICollectionReusableView *)collectionView:
 (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 return [[UICollectionReusableView alloc] init];
 }*/

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    _currentCourse = [_courses objectAtIndex:indexPath.row];
    [self.tableView reloadData];

    //Set up Labels:
    self.courseNameLabel.text = [_currentCourse.name uppercaseString];
    self.courseCodeLabel.text = [_currentCourse.code uppercaseString];
    self.courseStatusCode.text = [_currentCourse.statusCode uppercaseString];
    self.courseFeeStatus.text = _currentCourse.feeFreeFlag ? @"YES" : @"NO";
    
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: Deselect item
}


#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(206, 96);
}

// 3
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(20, 20, 20, 20);
}


@end
