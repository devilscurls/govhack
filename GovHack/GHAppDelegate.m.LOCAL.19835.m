//
//  GHAppDelegate.m
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHAppDelegate.h"

@implementation GHAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    GHProvidersParser *providerParser = [[GHProvidersParser alloc] init];
    self.providers = [providerParser parse];

    GHUnitsOfCompetencyParser *unitsOfCompetencyParser = [[GHUnitsOfCompetencyParser alloc] init];
    self.unitsOfCompetency = [unitsOfCompetencyParser parse];
    
    GHOccupationsParser *occupationsParser = [[GHOccupationsParser alloc] init];
    self.occupations = [occupationsParser parse];
    
    GHIndustriesParser *industriesParser = [[GHIndustriesParser alloc] init];
    self.industries = [industriesParser parse];
    
    GHCoursesParser *coursesParser = [[GHCoursesParser alloc] init];
    self.courses = [coursesParser parse];
    

    
    
    for (GHCourse *course in self.courses)
    {
        if (course.occupation && ![course.industry.occupations containsObject:course.occupation])
        {
            [course.industry.occupations addObject:course.occupation];
        }
    }
    
    
    
    NSLog(@"%@", self.industries);
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
