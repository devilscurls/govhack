//
//  GHOccupationViewController.m
//  GovHack
//
//  Created by Lewis Daly on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHOccupationViewController.h"
#import "GHCoursesViewController.h"

@interface GHOccupationViewController ()

@end

@implementation GHOccupationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden = NO;
    
    //Delegates
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    
    self.navigationController.navigationBarHidden = YES;
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.titleLabel setAdjustsFontSizeToFitWidth:YES];
    self.titleLabel.minimumScaleFactor = 0.5;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //Set the Title
    NSString *prefix = [self.currentIndustry.name uppercaseString];
    [self.titleLabel setText:[NSString stringWithFormat:@"%@ - OCCUPATIONS", prefix]];

}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions
- (IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - Prepare for Segue
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowCourses"])
    {
        //Set up the next view
        GHCoursesViewController *courcesVC = segue.destinationViewController;
        
        //Get Selected Occupation
        NSIndexPath *indexPath = [[_collectionView indexPathsForSelectedItems] lastObject];
        GHOccupation *occupation = [_currentIndustry.occupations objectAtIndex:indexPath.row];
        
        courcesVC.courses = occupation.coursesForOccupation;
        
        
    }
    
}



#pragma mark - UICollectionView Datasource
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return _currentIndustry.occupations.count;
    
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 1;
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GHOccupationCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"OccupationCell" forIndexPath:indexPath];
    
    //Get The occupation object:
    GHOccupation *occupation = [_currentIndustry.occupations objectAtIndex:indexPath.row];
    
    //Set up cell
    cell.titleLabel.text = occupation.name;
    cell.valueLabel.text = occupation.value;
    
    return cell;
}

// 4
/*- (UICollectionReusableView *)collectionView:
 (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 return [[UICollectionReusableView alloc] init];
 }*/

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //Perform Segue
    //No need to perform segue - this is done for us!
//    [self performSegueWithIdentifier:@"ShowCourses" sender:self];
    

    
    
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: Deselect item
}


#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(206, 96);
}


- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(20, 20, 20, 20);
}

@end
