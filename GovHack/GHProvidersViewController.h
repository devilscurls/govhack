//
//  GHProvidersViewController.h
//  GovHack
//
//  Created by Lewis Daly on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GHProvider.h"
#import "GHCourse.h"
#import "GHCells.h"

@interface GHProvidersViewController : UIViewController <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) GHCourse *currentCourse;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
- (IBAction)backButtonPressed:(id)sender;

@end
