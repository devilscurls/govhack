//
//  GHProviderTableViewCell.h
//  GovHack
//
//  Created by Lewis Daly on 2/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GHProviderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
@property (weak, nonatomic) IBOutlet UIView *cellBackgroundView;

@end
