//
//  GHOccupationViewController.h
//  GovHack
//
//  Created by Lewis Daly on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GHOccupation.h"
#import "GHIndustry.h"  
#import "GHCells.h" 

@interface GHOccupationViewController : UIViewController <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>


@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) GHIndustry *currentIndustry; //Keep track of current industry
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (IBAction)backButtonPressed:(id)sender;

@end
