//
//  GHProvidersParser.m
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHIndustriesParser.h"

@implementation GHIndustriesParser

- (NSArray *)parse
{
    self.desiredIndustries = @[
      //      @"Mathematical Sciences",
      //      @"Physics And Astronomy",
            @"Chemical Sciences",
            @"Earth Sciences",
            @"Other Natural And Physical Sciences",
            @"Information Systems",
            @"Other Information Technology",
    //        @"Manufacturing Engineering And Technology",
            @"Process And Resources Engineering",
            @"Automotive Engineering And Technology",
            @"Mechanical And Industrial Engineering And Technology",
            @"Civil Engineering",
            @"Geomatic Engineering",
            @"Electrical And Electronic Engineering And Technology",
            @"Other Engineering And Related Technologies",
            @"Architecture And Urban Environment",
            @"Building",
            @"Environmental Studies",
            @"Other Agriculture, Environmental And Related Studies",
            @"Accounting",
            @"Business And Management",
            @"Banking, Finance And Related Fields",
            @"Other Management And Commerce",
            @"Economics And Econometrics",
        //    @"Food And Hospitality",
            @"Personal Services",
            @"Employment Skills Programmes",
            @"Other Mixed Field Programmes"
        ];
    
    self.industries = [NSMutableArray array];
    
    NSString* path = [[NSBundle mainBundle] pathForResource:@"Industries" ofType:@"xml"];
    
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:path];
    
    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:xmlData];
    parser.delegate = self;
    
    [parser parse];
    
    return self.industries;
}


-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"Industry"])
    {
        self.currentIndustry = [[GHIndustry alloc] init];
    }
    else
    {
        self.currentElement = elementName;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if ([self.currentElement isEqualToString:@"IndustryName"])
    {
        if ([self.desiredIndustries containsObject:string])
        {
            self.currentIndustry.name = string;
        }
        else
        {
            self.currentIndustry = nil;
        }
    }
    if ([self.currentElement isEqualToString:@"IndustryValue"])
    {
        self.currentIndustry.value = string;
    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"Industry"])
    {
        if (self.currentIndustry)
        {
            [self.industries addObject:self.currentIndustry];
        }
        self.currentIndustry = nil;
    }
}


@end
