//
//  GHProvidersParser.h
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GHCourse.h"
#import "GHUnitOfCompetency.h"
#import "GHIndustry.h"
#import "GHOccupation.h"
#import "GHProvider.h"


@class GHAppDelegate;
@interface GHCoursesParser : NSObject <NSXMLParserDelegate>


@property (strong, nonatomic) GHCourse *currentCourse;
@property (strong, nonatomic) NSString *currentElement;
@property (strong, nonatomic) NSMutableArray *courses;
@property (weak, nonatomic) GHAppDelegate *appDelegate;
- (NSArray *)parse;


@property (readwrite) NSInteger counter;
@end
