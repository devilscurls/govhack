//
//  GHProvidersParser.m
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHProvidersParser.h"

@implementation GHProvidersParser

- (NSArray *)parse
{
    self.providers = [NSMutableArray array];
    
    NSString* path = [[NSBundle mainBundle] pathForResource:@"Providers" ofType:@"xml"];
    
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:path];
    
    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:xmlData];
    parser.delegate = self;
    
    [parser parse];
    
    NSLog(@"self.providers %d", [self.providers count]);
    return self.providers;
}


-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"Provider"])
    {
        self.currentProvider = [[GHProvider alloc] init];
    }
    else
    {
        self.currentElement = elementName;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if ([self.currentElement isEqualToString:@"ProviderID"])
    {
        self.currentProvider.guid = string;
    }
    if ([self.currentElement isEqualToString:@"ProviderName"])
    {
        self.currentProvider.name = string;
    }
    if ([self.currentElement isEqualToString:@"Email"])
    {
        self.currentProvider.email = string;
    }
    if ([self.currentElement isEqualToString:@"WorkPhone"])
    {
        self.currentProvider.workPhone = string;
    }
    if ([self.currentElement isEqualToString:@"WebAddress"])
    {
        self.currentProvider.urlString = string;
    }
    if ([self.currentElement isEqualToString:@"SkillsForAllApproved"])
    {
        self.currentProvider.skillsForAllApproved = [string isEqualToString:@"Yes"];
    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"Provider"])
    {
        [self.providers addObject:self.currentProvider];
        self.currentProvider = nil;
    }
}


@end
