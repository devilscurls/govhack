//
//  GHProvidersParser.m
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHCoursesParser.h"
#import "GHAppDelegate.h"

@implementation GHCoursesParser

- (NSArray *)parse
{
    self.counter = 0;
    
    self.courses = [NSMutableArray array];
    self.appDelegate = (GHAppDelegate *)[[UIApplication sharedApplication] delegate];

    NSString* path = [[NSBundle mainBundle] pathForResource:@"Courses" ofType:@"xml"];
    
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:path];
    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:xmlData];
    parser.delegate = self;
    
    [parser parse];

    NSLog(@"%d", [self.courses count]);
    NSLog(@"counter %d", self.counter);
    return self.courses;
}


-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"Course"])
    {
        self.currentCourse = [[GHCourse alloc] init];
        self.currentCourse.unitsOfCompetency = [NSMutableArray array];
    }
    else
    {
        self.currentElement = elementName;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if ([self.currentElement isEqualToString:@"CourseID"])
    {
        self.currentCourse.guid = string;
    }
    if ([self.currentElement isEqualToString:@"CourseCode"])
    {
        self.currentCourse.code = string;
    }
    if ([self.currentElement isEqualToString:@"CourseName"])
    {
        self.currentCourse.name = string;
    }
    if ([self.currentElement isEqualToString:@"AFQQualificationID"])
    {
        self.currentCourse.qualificationID = string;
    }
    if ([self.currentElement isEqualToString:@"EnrolmentCutOffDate"])
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        
        self.currentCourse.enrolmentCutOffDate = [formatter dateFromString:string];
    }
    if ([self.currentElement isEqualToString:@"StatusCode"])
    {
        self.currentCourse.statusCode = string;
    }
    if ([self.currentElement isEqualToString:@"FeeFreeFlag"])
    {
        self.currentCourse.feeFreeFlag = [string isEqualToString:@"Yes"];
    }
    if ([self.currentElement isEqualToString:@"UnitOfCompetencyID"])
    {
//        if (![self.currentCourse.unitsOfCompetency containsObject:string])
//        {
//            [self.currentCourse.unitsOfCompetency addObject:string];
//        }
//        else
//        {
//            NSLog(@"duplicate");
//        }
        if (self.currentCourse != nil)
        {
            GHUnitOfCompetency *unitOfCompetency = [self unitOfCompetencyForID:string];
            if (unitOfCompetency)
            {
                [self.currentCourse.unitsOfCompetency addObject:unitOfCompetency];
            }
            else
            {
                NSLog(@"unit of competency not found %@", unitOfCompetency);
            }
        }
    }

    if ([self.currentElement isEqualToString:@"ProviderID"])
    {
        if (self.currentCourse != nil)
        {
            GHProvider *provider = [self providerForGuid:string];
            if (provider)
            {
                [self.currentCourse.providers addObject:provider];
            }
            else
            {
                NSLog(@"provider not found %@", string);
            }
        }
    }
    
    if ([self.currentElement isEqualToString:@"IndustryValue"])
    {
        //        if (self.currentCourse.industry != nil)
        //        {
        //            NSLog(@"ind clash %@ %@ %@", self.currentCourse.industry.value, string, nil);
        //        }

        if (self.currentCourse != nil)
        {
            self.currentCourse.industry = [self industryForValue:string];
            if (self.currentCourse.industry == nil) {
                self.currentCourse = nil;
            }
        }
    }
    if ([self.currentElement isEqualToString:@"OccupationValue"])
    {
        //        if (self.currentCourse.occupation != nil)
        //        {
        //            NSLog(@"occ clash %@ %@", self.currentCourse.occupation.value, string);
        //
        //        }

        if (self.currentCourse != nil)
        {
            self.currentCourse.occupation = [self occupationForValue:string];
        }
    }

}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"Course"])
    {
        if (self.currentCourse != nil)
        {
            [self.courses addObject:self.currentCourse];
        }
        self.currentCourse = nil;
    }
}

- (GHUnitOfCompetency *)unitOfCompetencyForID:(NSString *)guid
{
    for (GHUnitOfCompetency *unitOfCompetency in self.appDelegate.unitsOfCompetency)
    {
        if ([unitOfCompetency.guid isEqualToString:guid])
        {
            return unitOfCompetency;
        }
    }
    
    return nil;
}

- (GHProvider *)providerForGuid:(NSString *)guid
{
    for (GHProvider *provider in self.appDelegate.providers)
    {
        if ([provider.guid isEqualToString:guid])
        {
            return provider;
        }
    }
    return nil;
}

- (GHIndustry *)industryForValue:(NSString *)value
{
    for (GHIndustry *industry in self.appDelegate.industries)
    {
        if ([industry.value isEqualToString:value])
        {
            return industry;
        }
    }
    return nil;
}


- (GHOccupation *)occupationForValue:(NSString *)value
{
    for (GHOccupation *occupation in self.appDelegate.occupations)
    {
        if ([occupation.value isEqualToString:value])
        {
            return occupation;
        }
    }
    return nil;
}

@end
