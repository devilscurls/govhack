//
//  GHProvidersParser.m
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHUnitsOfCompetencyParser.h"

@implementation GHUnitsOfCompetencyParser

- (NSArray *)parse
{
    self.unitsOfCompetency = [NSMutableArray array];
    
    NSString* path = [[NSBundle mainBundle] pathForResource:@"UnitOfCompetencies" ofType:@"xml"];
    
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:path];
    
    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:xmlData];
    parser.delegate = self;
    
    [parser parse];
    
    NSLog(@"self.unitsOfCompetency %d", [self.unitsOfCompetency count]);
    return self.unitsOfCompetency;
}


-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"UnitOfCompetency"])
    {
        self.currentUnitOfCompetency= [[GHUnitOfCompetency alloc] init];
    }
    else
    {
        self.currentElement = elementName;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if ([self.currentElement isEqualToString:@"UnitOfCompetencyID"])
    {
        self.currentUnitOfCompetency.guid = string;
    }
    if ([self.currentElement isEqualToString:@"UnitOfCompetencyCode"])
    {
        self.currentUnitOfCompetency.code = string;
    }
    if ([self.currentElement isEqualToString:@"UnitOfCompetencyName"])
    {
        self.currentUnitOfCompetency.name = string;
    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"UnitOfCompetency"])
    {
        [self.unitsOfCompetency addObject:self.currentUnitOfCompetency];
        self.currentUnitOfCompetency = nil;
    }
}


@end
