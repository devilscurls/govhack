//
//  GHProvidersParser.m
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHOccupationsParser.h"

@implementation GHOccupationsParser

- (NSArray *)parse
{
    self.occupations = [NSMutableArray array];
    
    NSString* path = [[NSBundle mainBundle] pathForResource:@"Occupations" ofType:@"xml"];
    
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:path];
    
    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:xmlData];
    parser.delegate = self;
    
    [parser parse];
    
    NSLog(@"self.occupations %d", [self.occupations  count]);
    return self.occupations;
}


-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"Occupation"])
    {
        self.currentOccupation= [[GHOccupation alloc] init];
    }
    else
    {
        self.currentElement = elementName;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if ([self.currentElement isEqualToString:@"OccupationName"])
    {
        self.currentOccupation.name = string;
    }
    if ([self.currentElement isEqualToString:@"OccupationValue"])
    {
        self.currentOccupation.value = string;
    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"Occupation"])
    {
        [self.occupations addObject:self.currentOccupation];
        self.currentOccupation = nil;
    }
}


@end
