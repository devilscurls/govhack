//
//  GHProvidersParser.h
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GHProvider.h"
@interface GHProvidersParser : NSObject <NSXMLParserDelegate>

@property (strong, nonatomic) GHProvider *currentProvider;
@property (strong, nonatomic) NSString *currentElement;
@property (strong, nonatomic) NSMutableArray *providers;

- (NSArray *)parse;

@end
