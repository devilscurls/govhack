//
//  GHProvidersParser.h
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GHOccupation.h"
@interface GHOccupationsParser : NSObject <NSXMLParserDelegate>

@property (strong, nonatomic) GHOccupation *currentOccupation;
@property (strong, nonatomic) NSString *currentElement;
@property (strong, nonatomic) NSMutableArray *occupations;

- (NSArray *)parse;

@end
