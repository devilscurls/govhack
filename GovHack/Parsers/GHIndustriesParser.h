//
//  GHProvidersParser.h
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GHIndustry.h"
@interface GHIndustriesParser : NSObject <NSXMLParserDelegate>

@property (strong, nonatomic) GHIndustry *currentIndustry;
@property (strong, nonatomic) NSString *currentElement;
@property (strong, nonatomic) NSMutableArray *industries;
@property (strong, nonatomic) NSArray *desiredIndustries;
- (NSArray *)parse;

@end
