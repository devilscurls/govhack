//
//  GHProvidersParser.h
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GHUnitOfCompetency.h"

@interface GHUnitsOfCompetencyParser : NSObject <NSXMLParserDelegate>

@property (strong, nonatomic) GHUnitOfCompetency *currentUnitOfCompetency;
@property (strong, nonatomic) NSString *currentElement;
@property (strong, nonatomic) NSMutableArray *unitsOfCompetency;

- (NSArray *)parse;

@end
