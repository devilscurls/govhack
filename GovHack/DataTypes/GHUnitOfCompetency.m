//
//  GHUnitOfCompetency.m
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHUnitOfCompetency.h"

@implementation GHUnitOfCompetency

- (void) encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.code forKey:@"code"];
    [encoder encodeObject:self.guid forKey:@"guid"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self)
    {
        self.guid = [decoder decodeObjectForKey:@"guid"];
        self.name = [decoder decodeObjectForKey:@"code"];
        self.guid = [decoder decodeObjectForKey:@"guid"];
    }
    return self;
}




- (NSString *)description
{
    return [NSString stringWithFormat:@"<GHUnitOfCompetency> %@ %@", self.name, self.code];
}
@end
