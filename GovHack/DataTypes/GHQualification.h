//
//  GHQualification.h
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GHQualification : NSObject <NSCoding>

@property (strong, nonatomic) NSString *guid;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *priorityClassification;
@property (strong, nonatomic) NSString *courseQualificationOrSkillSet;

@property (readwrite) BOOL certificate1Or2;
@property (readwrite) BOOL trainingGuarantee;
@property (readwrite) BOOL feeFree;
@property (readwrite) BOOL priority;
@property (readwrite) BOOL availableUnderTraineeshipApprenticeship;

@end
