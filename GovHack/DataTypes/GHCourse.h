//
//  GHCourse.h
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GHIndustry.h"
#import "GHOccupation.h"
@interface GHCourse : NSObject <NSCoding>

@property (strong, nonatomic) NSString *guid;
@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *qualificationID;
//AFQQualificationName (qtep_aqfqualificationlevelidname from Approved Course/Qualification entity)

@property (strong, nonatomic) NSDate *enrolmentCutOffDate;
@property (strong, nonatomic) NSString *statusCode;
@property (readwrite) BOOL feeFreeFlag;
@property (strong, nonatomic) NSMutableArray *unitsOfCompetency;
@property (strong, nonatomic) GHIndustry *industry;
@property (strong, nonatomic) GHOccupation *occupation;
@property (strong, nonatomic) NSMutableArray *providers;

@end
