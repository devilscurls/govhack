//
//  GHProvider.h
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GHProvider : NSObject <NSCoding>

@property (strong, nonatomic) NSString *guid;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *workPhone;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *urlString;
@property (readwrite) BOOL skillsForAllApproved;

@end
