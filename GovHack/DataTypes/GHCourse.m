//
//  GHCourse.m
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHCourse.h"

@implementation GHCourse

- (id)init
{
    self = [super init];
    if (self)
    {
        self.industry = nil;
        self.occupation = nil;
        self.unitsOfCompetency = [NSMutableArray array];
        self.providers = [NSMutableArray array];
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.guid forKey:@"guid"];
    [encoder encodeObject:self.code forKey:@"code"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.qualificationID forKey:@"qualificationID"];
    [encoder encodeObject:self.enrolmentCutOffDate forKey:@"enrolmentCutOffDate"];
    [encoder encodeObject:self.statusCode forKey:@"statusCode"];
    [encoder encodeBool:self.feeFreeFlag forKey:@"feeFreeFlag"];
    [encoder encodeObject:self.unitsOfCompetency forKey:@"unitsOfCompetency"];
    [encoder encodeObject:self.industry forKey:@"industry"];
    [encoder encodeObject:self.occupation forKey:@"occupation"];
    [encoder encodeObject:self.providers forKey:@"providers"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self)
    {
        self.guid = [decoder decodeObjectForKey:@"guid"];
        self.code = [decoder decodeObjectForKey:@"code"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.qualificationID = [decoder decodeObjectForKey:@"qualificationID"];
        self.enrolmentCutOffDate = [decoder decodeObjectForKey:@"enrolmentCutOffDate"];
        self.statusCode = [decoder decodeObjectForKey:@"statusCode"];
        self.feeFreeFlag = [decoder decodeBoolForKey:@"feeFreeFlag"];
        self.unitsOfCompetency = [decoder decodeObjectForKey:@"unitsOfCompetency"];
        self.industry = [decoder decodeObjectForKey:@"industry"];
        self.occupation = [decoder decodeObjectForKey:@"occupation"];
        self.providers = [decoder decodeObjectForKey:@"providers"];
    }
    return self;
}
- (NSString *)description
{
    return [NSString stringWithFormat:@"<GHCourse> %@ %@", self.name, self.guid];
}

@end
