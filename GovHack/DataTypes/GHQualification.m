//
//  GHQualification.m
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHQualification.h"

@implementation GHQualification


- (void) encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.guid forKey:@"guid"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.priorityClassification forKey:@"priorityClassification"];
    [encoder encodeObject:self.courseQualificationOrSkillSet forKey:@"courseQualificationOrSkillSet"];
    [encoder encodeBool:self.certificate1Or2 forKey:@"certificate1Or2"];
    [encoder encodeBool:self.trainingGuarantee forKey:@"trainingGuarantee"];
    [encoder encodeBool:self.feeFree forKey:@"feeFree"];
    [encoder encodeBool:self.priority forKey:@"priority"];
    [encoder encodeBool:self.availableUnderTraineeshipApprenticeship forKey:@"availableUnderTraineeshipApprenticeship"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (self)
    {
        self.guid = [decoder decodeObjectForKey:@"guid"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.priorityClassification = [decoder decodeObjectForKey:@"priorityClassification"];
        self.courseQualificationOrSkillSet = [decoder decodeObjectForKey:@"courseQualificationOrSkillSet"];
        self.certificate1Or2 = [decoder decodeBoolForKey:@"certificate1Or2"];
        self.trainingGuarantee = [decoder decodeBoolForKey:@"trainingGuarantee"];
        self.feeFree = [decoder decodeBoolForKey:@"feeFree"];
        self.priority = [decoder decodeBoolForKey:@"priority"];
        self.availableUnderTraineeshipApprenticeship = [decoder decodeBoolForKey:@"availableUnderTraineeshipApprenticeship"];
    }
    return self;
}




@end
