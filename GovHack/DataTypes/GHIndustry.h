//
//  GHIndustry.h
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GHIndustry : NSObject <NSCoding>

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *value;

@property (strong, nonatomic) NSMutableArray *occupations;

@end
