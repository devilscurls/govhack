//
//  GHIndustry.m
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHIndustry.h"

@implementation GHIndustry


- (id)init
{
    self = [super init];
    if (self)
    {
        self.occupations = [NSMutableArray array];
    }
    return self;
}


- (void) encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.value forKey:@"value"];
    [encoder encodeObject:self.occupations forKey:@"occupations"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self)
    {
        self.name = [decoder decodeObjectForKey:@"name"];
        self.value = [decoder decodeObjectForKey:@"value"];
        self.occupations = [decoder decodeObjectForKey:@"occupations"];
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<GHIndustry> %@ occupations %d", self.name, [self.occupations count]];
}

@end
