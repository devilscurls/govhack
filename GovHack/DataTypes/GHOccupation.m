//
//  GHOccupation.m
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHOccupation.h"
#import "GHAppDelegate.h"
#import "GHCourse.h"
@implementation GHOccupation

- (NSArray *)coursesForOccupation
{
    NSMutableArray *array = [NSMutableArray array];
    
    GHAppDelegate *appDelegate = (GHAppDelegate *)[[UIApplication sharedApplication] delegate];

    for (GHCourse *course in appDelegate.courses)
    {
        if (course.occupation == self)
        {
            [array addObject:course];
        }
    }
    
    NSLog(@"array %@",array);
    
    return array;
}

- (void) encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.value forKey:@"value"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self)
    {
        self.name = [decoder decodeObjectForKey:@"name"];
        self.value = [decoder decodeObjectForKey:@"value"];
    }
    return self;
}



- (NSString *)description
{
    return [NSString stringWithFormat:@"<GHOccupation> %@ %@", self.name, self.value];
}

@end
