//
//  GHUnitOfCompetency.h
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GHUnitOfCompetency : NSObject <NSCoding>

@property (strong, nonatomic) NSString *guid;
@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) NSString *name;

@end
