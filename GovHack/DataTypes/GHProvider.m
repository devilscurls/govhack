//
//  GHProvider.m
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHProvider.h"

@implementation GHProvider


- (void) encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.guid forKey:@"guid"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.workPhone forKey:@"workPhone"];
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.urlString forKey:@"urlString"];
    [encoder encodeBool:self.skillsForAllApproved forKey:@"skillsForAllApproved"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self)
    {
        self.guid = [decoder decodeObjectForKey:@"guid"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.workPhone = [decoder decodeObjectForKey:@"workPhone"];
        self.email = [decoder decodeObjectForKey:@"email"];
        self.urlString = [decoder decodeObjectForKey:@"urlString"];
        self.skillsForAllApproved = [decoder decodeBoolForKey:@"skillsForAllApproved"];
    }
    return self;
}


- (NSString *)description
{
    return [NSString stringWithFormat:@"<GHIndustry> %@ %@", self.name, self.guid];
}

@end
