//
//  GHRootViewController.h
//  GovHack
//
//  Created by Lewis Daly on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GHAppDelegate.h"
#import "GHCells.h"
#import <GCPlaceHolderTextView.h>

@interface GHRootViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) GCPlaceholderTextView *searchField; //TODO: Add shadows and crap
@property (weak, nonatomic) IBOutlet UIButton *searchButton;

- (IBAction)searchButtonPressed:(id)sender;

@end
