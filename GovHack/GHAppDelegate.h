//
//  GHAppDelegate.h
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GHProvidersParser.h"
#import "GHIndustriesParser.h"
#import "GHOccupationsParser.h"
#import "GHUnitsOfCompetencyParser.h"
#import "GHCoursesParser.h"

@interface GHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSArray *providers;
@property (strong, nonatomic) NSArray *unitsOfCompetency;
@property (strong, nonatomic) NSArray *courses;
@property (strong, nonatomic) NSArray *industries;
@property (strong, nonatomic) NSArray *occupations;

@end
