//
//  GHRootViewController.m
//  GovHack
//
//  Created by Lewis Daly on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHRootViewController.h"
#import "GHIndustry.h"
#import "GHOccupationViewController.h"
#import "GHCourse.h"
#import "GHProvider.h"
#import "GHOccupation.h"

@interface GHRootViewController ()

@property (nonatomic) BOOL isSearching; //keep track of searching!
@property (nonatomic, strong) NSMutableArray *matchedCourses;
@property (nonatomic, strong) NSMutableArray *matchedOccupations;
@property (nonatomic, strong) NSMutableArray *matchedIndustries;
@property (nonatomic, strong) NSMutableArray *matchedProviders;



@end

@implementation GHRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //Delegates
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    //Register Cell
    //    [self.collectionView registerClass:[GHIndustryCell class] forCellWithReuseIdentifier:@"IndustryCell"];
    
    //View Setup
    self.navigationController.navigationBarHidden = YES;
    self.collectionView.backgroundColor = [UIColor clearColor];
    //    self.collectionView.contentInset = UIEdgeInsetsMake(0, 20, 0, 20);
    
    //TODO: Fix this... cocoa pods issue
    //    [self.searchField setPlaceholder:@"SEARCH FOR OCCUPATION, COURSE"];
    //    [self.searchField setPlaceholderColor:[UIColor whiteColor]];
    
    self.searchField = [[GCPlaceholderTextView alloc] initWithFrame:CGRectMake(439, 60, 364, 30)];
    [self.searchField setPlaceholderColor:[UIColor whiteColor]];
    self.searchField.scrollEnabled = NO;
    [self.searchField setPlaceholder:@"SEARCH FOR AN OCCUPATION, COURSE"];
    [self.searchField setKeyboardType:UIKeyboardTypeASCIICapable];
    self.searchField.returnKeyType = UIReturnKeySearch;
    self.searchField.backgroundColor = [UIColor clearColor];
    [self.searchField setDelegate:self];
    [self.searchField setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18]];
    self.searchField.textColor = [UIColor whiteColor];
    
    [self.view addSubview:self.searchField];
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Prepare for segue
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"ShowOccupation"])
    {
        GHOccupationViewController *destinationVC = segue.destinationViewController;
        
        //Get the current index
        NSIndexPath *indexPath = [[_collectionView indexPathsForSelectedItems] lastObject];
        
        GHIndustry *industry = [[self industries] objectAtIndex:indexPath.row];
        destinationVC.currentIndustry = industry;
        
    }
    
}

#pragma mark - Search
- (void) performSearch
{
    GHAppDelegate *appDelegate = (GHAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSMutableArray *words = (NSMutableArray *)[self.searchField.text componentsSeparatedByString:@" "];
    //    [words removeObject:@" "];
    
    _matchedCourses = [NSMutableArray array];
    
    for (GHCourse *course in appDelegate.courses)
    {
        // assume match
        BOOL isMatch = true;
        
        for (NSString *word in words)
        {
            if ([course.name rangeOfString:word].location == NSNotFound)
            {
                isMatch = false;
            }
        }
        
        if (isMatch)
        {
            [_matchedCourses addObject:course];
        }
    }
    
    _matchedIndustries = [NSMutableArray array];
    
    for (GHIndustry *industry in appDelegate.industries)
    {
        // assume match
        BOOL isMatch = true;
        
        for (NSString *word in words)
        {
            if ([industry.name rangeOfString:word].location == NSNotFound)
            {
                isMatch = false;
            }
        }
        
        if (isMatch)
        {
            [_matchedIndustries addObject:industry];
        }
    }
    
    _matchedProviders = [NSMutableArray array];
    
    for (GHProvider *provider in appDelegate.providers)
    {
        // assume match
        BOOL isMatch = true;
        
        for (NSString *word in words)
        {
            if ([provider.name rangeOfString:word].location == NSNotFound)
            {
                isMatch = false;
            }
        }
        
        if (isMatch)
        {
            [_matchedProviders addObject:provider];
        }
    }
    
    _matchedOccupations = [NSMutableArray array];
    
    for (GHOccupation *occupation in appDelegate.occupations)
    {
        // assume match
        BOOL isMatch = true;
        
        for (NSString *word in words)
        {
            if ([occupation.name rangeOfString:word].location == NSNotFound)
            {
                isMatch = false;
            }
        }
        
        if (isMatch)
        {
            [_matchedOccupations addObject:occupation];
        }
    }
    
    
    NSLog(@"occupations %@", _matchedOccupations);
    NSLog(@"courses %@", _matchedCourses);
    NSLog(@"providers %@", _matchedProviders);
    NSLog(@"industries %@", _matchedIndustries);
    
    
    _isSearching = YES;
    [self.collectionView reloadData];
}

#pragma mark - IBActions
- (IBAction)searchButtonPressed:(id)sender
{
    if (_isSearching)
    {
        [self.searchButton setTitle:@"SEARCH" forState:UIControlStateNormal];
        _isSearching = NO;
        [self.collectionView reloadData];
    }
    else
    {
        [self.searchButton setTitle:@"DONE" forState:UIControlStateNormal];

    //Dismiss keyboard
    [self.searchField resignFirstResponder];
    
    //TODO: Perform Search
    [self performSearch];
    }
}

#pragma mark - UITextField Methods
- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [self.searchField resignFirstResponder];
        [self performSearch];
        [self.searchButton setTitle:@"DONE" forState:UIControlStateNormal];
        return NO;
    }
    return YES;
}

#pragma mark - UICollectionView Datasource
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    if(_isSearching)
    {
        switch (section)
        {
            case 0:
                return _matchedIndustries.count;
                break;
            case 1:
                return _matchedOccupations.count;
                break;
            case 2:
                return _matchedCourses.count;
                break;
            case 3:
                return _matchedProviders.count;
            default:
                break;
        }
        
    }
    else
        return [self industries].count;
    
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    if(_isSearching)
    {
        return 4;
    }
    
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (_isSearching)
    {
        GHIndustryCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"IndustryCell" forIndexPath:indexPath];
        
        switch (indexPath.section)
        {
            case 0:
            {
                //Load Industries
                GHIndustry *industry = [self.matchedIndustries objectAtIndex: indexPath.row];
                cell.titleLabel.text = industry.name;
            }
                break;
            case 1:
            {
                //Load Occupation
                GHOccupation *occupation = [self.matchedOccupations objectAtIndex:indexPath.row];
                cell.titleLabel.text = occupation.name;
                
            }
                break;
            case 2:
            {
                //Load Courses
                GHCourse *course = [self.matchedCourses objectAtIndex:indexPath.row];
                cell.titleLabel.text = course.name;
            }
                break;
            case 3:
            {
                //Load Providers
                GHProvider *provider = [self.matchedProviders objectAtIndex:indexPath.row];
                cell.titleLabel.text = provider.name;
            }
                break;
                
            default:
                break;
        }
        
        return cell;
    }
    else
    {
        GHIndustryCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"IndustryCell" forIndexPath:indexPath];
        
        //Get the Industy Object:
        GHIndustry *industry = [[self industries] objectAtIndex:indexPath.row];
        
        
        cell.titleLabel.text = [industry.name uppercaseString];
        return cell;
    }
}



#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //TODO: if searching do some stuff!
    
    
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // TODO: Deselect item
}


#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(206, 96);
}

// 3
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(20, 20, 20, 20);
}


#pragma mark - Utilities
- (NSArray *) industries
{
    GHAppDelegate *delegate = (GHAppDelegate *)[UIApplication sharedApplication].delegate;
    return delegate.industries;
}

@end
