//
//  GHProviderPopoverViewController.h
//  GovHack
//
//  Created by Lewis Daly on 2/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GHProvider.h"

@interface GHProviderPopoverViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UILabel *email;
@property (weak, nonatomic) IBOutlet UILabel *site;
@property (weak, nonatomic) IBOutlet UILabel *skillsForAll;

- (void) loadProvider:(GHProvider *)provider;
- (void) resize;

@end
