//
//  GHAppDelegate.m
//  GovHack
//
//  Created by Luke Scholefield on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHAppDelegate.h"

@implementation GHAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    NSError *error = nil;
//    [[NSFileManager defaultManager] createDirectoryAtPath:@"test" withIntermediateDirectories:YES attributes:nil error:&error];
// 
//    
//    NSString *dataPath = @"temp";

    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *directory = [paths objectAtIndex:0];
    
    NSString *dataPath = [[NSString alloc] initWithFormat:@"%@/%@", directory, @"data"];
    

    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        NSLog(@"start");
        
        
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"data" ofType:nil];

        NSLog(@"*bundlePath %@", bundlePath);
        NSError *error = nil;
        
        [[NSFileManager defaultManager] copyItemAtPath:bundlePath
                                                toPath:dataPath
                                                 error:&error];
        
        if (error != nil)
        {
            NSLog(@"%@", error);
        }
        
//        GHProvidersParser *providerParser = [[GHProvidersParser alloc] init];
//        self.providers = [providerParser parse];
//
//        GHUnitsOfCompetencyParser *unitsOfCompetencyParser = [[GHUnitsOfCompetencyParser alloc] init];
//        self.unitsOfCompetency = [unitsOfCompetencyParser parse];
//        
//        GHOccupationsParser *occupationsParser = [[GHOccupationsParser alloc] init];
//        self.occupations = [occupationsParser parse];
//        
//        GHIndustriesParser *industriesParser = [[GHIndustriesParser alloc] init];
//        self.industries = [industriesParser parse];
//        
//        GHCoursesParser *coursesParser = [[GHCoursesParser alloc] init];
//        self.courses = [coursesParser parse];
//        
//    
//        NSMutableData *data = [[NSMutableData alloc] init];
//        NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
//        [archiver encodeObject:self.providers forKey:@"providers"];
//        [archiver encodeObject:self.unitsOfCompetency forKey:@"unitsOfCompetency"];
//        [archiver encodeObject:self.occupations forKey:@"occupations"];
//        [archiver encodeObject:self.industries forKey:@"industries"];
//        [archiver encodeObject:self.courses forKey:@"courses"];
//        [archiver finishEncoding];
//        [data writeToFile:dataPath atomically:YES];
    }
    
 //   else
 //   {
        NSLog(@"data from nscoder");
        
        NSData *codedData = [[NSData alloc] initWithContentsOfFile:dataPath];
//        if (codedData == nil) return nil;
        
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:codedData];
        
        self.providers = [unarchiver decodeObjectForKey:@"providers"];
        self.unitsOfCompetency = [unarchiver decodeObjectForKey:@"unitsOfCompetency"];
        self.occupations = [unarchiver decodeObjectForKey:@"occupations"];
        self.industries = [unarchiver decodeObjectForKey:@"industries"];
        self.courses = [unarchiver decodeObjectForKey:@"courses"];
        [unarchiver finishDecoding];
  //  }
    NSLog(@"self.course.count %d", [self.courses count]);
    
    
    for (GHCourse *course in self.courses)
    {
        if (course.occupation && ![course.industry.occupations containsObject:course.occupation])
        {
            [course.industry.occupations addObject:course.occupation];
        }
    }    
    
    GHCourse *course = nil;
    
    for (GHCourse *possibleCourse in self.courses) {
        NSLog(@"%d", [possibleCourse.unitsOfCompetency count]);
        if ([possibleCourse.unitsOfCompetency count] > 5)
        {
            course = possibleCourse;
            break;
        }
    }
    NSMutableArray *relatedCourses = [NSMutableArray array];
    
    NSLog(@"test course %@ %d", course, [course.unitsOfCompetency  count]);
    for (GHCourse *possibleMatch in self.courses)
    {
        if (possibleMatch.industry == nil)
        {
            continue;
        }
        
        for (GHUnitOfCompetency *unitOfCompetency in  course.unitsOfCompetency)
        {
           if ([possibleMatch.unitsOfCompetency containsObject:unitOfCompetency])
           {
               [relatedCourses addObject:possibleMatch];
               NSLog(@"%@ %@", possibleMatch.name, possibleMatch.industry.name);
               break;
           }
        }
    }
    
    NSLog(@"\n\n\n\n");
//    
//    
//    for (NSString *unitOfCompetencyForCourse in  course.unitsOfCompetency)
//    {
//        for (GHUnitOfCompetency * unitOfCompetency in self.unitsOfCompetency)
//        {
//            if ([unitOfCompetency.guid isEqualToString:unitOfCompetencyForCourse])
//            {
//                NSLog(@"%@", unitOfCompetency.name);
//            }
//        }
//    }
//    
    
//    NSLog(@"related courses %@", relatedCourses);
//    
//    NSLog(@"%@", [self.occupations objectAtIndex:11]);
//    [(GHOccupation *)[self.occupations objectAtIndex:11] coursesForOccupation];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
