//
//  GHCourseCell.m
//  GovHack
//
//  Created by Lewis Daly on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "GHCourseCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation GHCourseCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    self.backgroundColor = [UIColor clearColor];
    self.backgroundView.backgroundColor = [UIColor blackColor];
    self.backgroundView.alpha = 0.5;
    self.backgroundView.layer.cornerRadius = 5;
    
    
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:20];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.frame = CGRectMake(10, 10, 186, 76);
    
    [self bringSubviewToFront:_titleLabel];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
