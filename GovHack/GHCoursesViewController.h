//
//  GHCoursesViewController.h
//  GovHack
//
//  Created by Lewis Daly on 1/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GHOccupation.h"
#import "GHCells.h"
#import "GHCourse.h"

@interface GHCoursesViewController : UIViewController <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *courses;
@property (nonatomic, strong) GHCourse *currentCourse;
@property (nonatomic, strong) GHOccupation *currentOccupation;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

//Right Hand Side Views
@property (weak, nonatomic) IBOutlet UILabel *courseNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseStatusCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseStatusCode;
@property (weak, nonatomic) IBOutlet UILabel *courseFeeStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *courseFeeStatus;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)backButtonPressed:(id)sender;

@end
